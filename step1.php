<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Dynamic Question Paper Generator - Step 1</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Dynamic Question Paper Generator - Step 1</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="process.php?call=step1">
                    <div class="form-row">
                        <div class="name">Institute Name</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="institute_name" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Exam Name</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="exam_name" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Logo</div>
                            <div class="value">
                                <div class="input-group">
                                    <input type="hidden" name="logo_id" id="logo_id" value="" >
                                    <input class="input--style-5" type="file" id="logo" name="logo" onchange="upload_file(this.id)" >
                                </div>
                            </div>
                        </div>
                        <div class="form-row m-b-55">
                            <div class="name">Course</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-3">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" name="course_code" required>
                                            <label class="label--desc">Course Code</label>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" name="course_name" required>
                                            <label class="label--desc">Course Name</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Branch</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="branch" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-row m-b-55">
                            <div class="name">Test Date</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-3">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="number" name="test_time" placeholder="Min." required>
                                            <label class="label--desc">Time</label>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="date" name="test_date" style="height:35px" required>
                                            <label class="label--desc">Test Date</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            </br></br>
                            <button class="btn btn--radius-2 btn--green" type="submit" name="action" value="next" style="margin-left:160px">Next </button>
                            <button class="btn btn--radius-2 btn--red" type="submit" name="action" value="save" style="margin-left:40px"> Finish & Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
<!-- end document-->