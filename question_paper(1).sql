-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2019 at 11:18 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `question_paper`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_question`
--

CREATE TABLE `mst_question` (
  `m_ques_id` int(11) NOT NULL,
  `ques_type` varchar(250) NOT NULL,
  `subject_ques` varchar(250) NOT NULL,
  `question` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_question`
--

INSERT INTO `mst_question` (`m_ques_id`, `ques_type`, `subject_ques`, `question`) VALUES
(10, 'Descriptive', 'Math', '<p>b) Find the electric field at a point (1, -2, 1)m, if the potential is V =3x2y + 2yz2\r\n+ 2xyz.</p>\r\n'),
(11, 'Descriptive', 'Math', '<p>2) a) State and explain Guass Law.<br />\r\nb) Find the tortal charge Q within the sphere of radius r=4m, if its volume charge density is (10/r sin&theta;) C/m3</p>\r\n'),
(12, 'Descriptive', 'Physic', '<p>Derive the expression for Electric Field Intensity due to a sheet of charge.</p>\r\n'),
(13, 'Descriptive', 'Math', '<p>&nbsp;State and explain Biot-Savart Law, Ampere Law.</p>\r\n'),
(14, 'Descriptive', 'Language', '<p>What is Inheritance</p>\r\n'),
(15, 'Descriptive', 'Language', '<p>What four pillar of opps?</p>\r\n'),
(16, 'Descriptive', 'Language', '<p>Describe ACID Properties ?</p>\r\n'),
(17, 'Descriptive', 'Language', '<p>Difference Between GET and Post Method</p>\r\n'),
(18, 'Descriptive', 'Math', '<p>The condition that the electrostatic field is conservative is</p>\r\n'),
(19, '', 'Physic', '<p>&nbsp;Derive the expression for the force exerted by a moving charge.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `qp_details`
--

CREATE TABLE `qp_details` (
  `qp_id` int(11) NOT NULL,
  `institute_name` varchar(255) DEFAULT NULL,
  `exam_name` varchar(100) DEFAULT NULL,
  `logo_url` varchar(100) DEFAULT NULL,
  `course_name` varchar(50) DEFAULT NULL,
  `course_code` varchar(20) DEFAULT NULL,
  `test_time` smallint(4) DEFAULT NULL COMMENT 'In minutes',
  `branch` varchar(50) DEFAULT NULL,
  `max_marks` smallint(4) DEFAULT NULL COMMENT 'Calculate in Backend',
  `test_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qp_details`
--

INSERT INTO `qp_details` (`qp_id`, `institute_name`, `exam_name`, `logo_url`, `course_name`, `course_code`, `test_time`, `branch`, `max_marks`, `test_date`) VALUES
(18, 'ASHOKA INSTITUTE OF ENGINEERING AND TECHNOLOGY', 'Test1 Internal Examination', 'upload/logo_1575017849.png', 'Electromagnetic Fields', 'EE305PC', 2, 'EEE B.Tech II-I(none)', NULL, '2019-11-26'),
(22, 'DY patil college of engg', 'Semister', 'upload/logo_1575018836.png', 'Electromagnetic Fields', 'BCS-101', 1, 'ETC-1233', NULL, '2019-11-29');

-- --------------------------------------------------------

--
-- Table structure for table `qp_sections`
--

CREATE TABLE `qp_sections` (
  `qp_sec_id` int(11) NOT NULL,
  `qp_id` int(11) DEFAULT NULL,
  `section_title` varchar(20) DEFAULT NULL COMMENT 'eg. A/B',
  `section_type` varchar(100) NOT NULL,
  `section_instruction` varchar(100) DEFAULT NULL,
  `how_much` int(11) NOT NULL,
  `out_of` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qp_sections`
--

INSERT INTO `qp_sections` (`qp_sec_id`, `qp_id`, `section_title`, `section_type`, `section_instruction`, `how_much`, `out_of`) VALUES
(22, 18, 'A', 'Mandatory', '', 0, 0),
(23, 18, 'B', 'Optional', '', 2, 3),
(24, 18, 'c', 'OR', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `questions_details`
--

CREATE TABLE `questions_details` (
  `qs_id` int(11) NOT NULL,
  `qp_id` int(11) DEFAULT NULL,
  `qp_sec_id` int(11) DEFAULT NULL,
  `qs_number` varchar(5) DEFAULT NULL,
  `qs_sub_number` varchar(5) DEFAULT NULL,
  `m_ques_id` int(11) DEFAULT NULL,
  `max_marks` smallint(6) DEFAULT NULL,
  `co_mapping` varchar(50) DEFAULT NULL,
  `diff_level` varchar(50) DEFAULT NULL,
  `is_or` int(11) NOT NULL,
  `is_optional` int(11) NOT NULL,
  `how_much` int(11) NOT NULL,
  `out_of` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_details`
--

INSERT INTO `questions_details` (`qs_id`, `qp_id`, `qp_sec_id`, `qs_number`, `qs_sub_number`, `m_ques_id`, `max_marks`, `co_mapping`, `diff_level`, `is_or`, `is_optional`, `how_much`, `out_of`) VALUES
(41, 18, 22, '1 a)', NULL, 10, 4, 'co-1', 'L1', 0, 0, 0, 0),
(42, 18, 22, '2 b)', NULL, 11, 4, 'co-2', 'L2', 0, 0, 0, 0),
(43, 18, 22, '2 c)', NULL, 12, 4, 'co-3', 'L3', 0, 0, 0, 0),
(44, 18, 23, '2 a)', NULL, 14, 5, 'co1-co2', 'L1', 0, 0, 0, 0),
(45, 18, 23, '2 b)', NULL, 15, 5, 'co3', 'L2', 0, 0, 0, 0),
(46, 18, 23, '2 c)', NULL, 16, 5, 'Co2', 'L4', 0, 0, 0, 0),
(47, 18, 24, '1a', NULL, 18, 8, 'co1', 'L1', 1, 0, 0, 0),
(48, 18, 24, '1a', NULL, 19, 8, 'co2', 'L2', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `password`, `user_status`) VALUES
(1, 'admin', 'admin', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_question`
--
ALTER TABLE `mst_question`
  ADD PRIMARY KEY (`m_ques_id`);

--
-- Indexes for table `qp_details`
--
ALTER TABLE `qp_details`
  ADD PRIMARY KEY (`qp_id`);

--
-- Indexes for table `qp_sections`
--
ALTER TABLE `qp_sections`
  ADD PRIMARY KEY (`qp_sec_id`);

--
-- Indexes for table `questions_details`
--
ALTER TABLE `questions_details`
  ADD PRIMARY KEY (`qs_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_question`
--
ALTER TABLE `mst_question`
  MODIFY `m_ques_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `qp_details`
--
ALTER TABLE `qp_details`
  MODIFY `qp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `qp_sections`
--
ALTER TABLE `qp_sections`
  MODIFY `qp_sec_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `questions_details`
--
ALTER TABLE `questions_details`
  MODIFY `qs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
