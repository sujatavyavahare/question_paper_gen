<?php
session_start();
define('FPDF_FONTPATH','fonts/');
include "pdf/mc_table.php";
include "conn.php";
$sql = 'SELECT qp_details.*, date_format(test_date, "%d-%m-%Y") AS exam_date FROM qp_details 
WHERE qp_details.qp_id='.$_GET["qp_id"].'';
$result1 = mysqli_query($conn, $sql);

$row1 = mysqli_fetch_assoc($result1);
$pdf=new PDF_MC_Table("P","pt","A4");
$pdf->Open();
$pdf->AddPage();
if(!empty(str_replace("upload/","",$row1['logo_url'])))
$pdf->Image("".$row1['logo_url'],45,15,90,60);
$pdf->SetFont("Arial","B",16);
$pdf->setx(120);
$pdf->SetAligns(array("C","C",));

$pdf->Row2(array($row1["institute_name"]),20);
$pdf->SetFont("Arial","B",14);
$pdf->Cell(540,12,$row1["branch"]." ".$row1['exam_name'],0,1,"C");

if($row1["test_time"] >= 60) {
	$time = round($row1["test_time"]/60) ." Hrs";
} else {
	$time = $row1["test_time"]." Min";
}

$pdf->Cell(540,15," ",0,1,"C");
$pdf->setY(85);
$pdf->Rect(40,85,540,750);
$pdf->SetFont("Arial","B",12);
$pdf->Cell(280,20,"  Course : ".$row1["course_name"],0,0,"L");
$pdf->Cell(150,20,"Course Code : ".$row1["course_code"],0,0,"L");
$pdf->Cell(200,20,"Time : ". $time,0,1,"L");
$pdf->Cell(280,20,"  Branch : ".$row1["branch"],0,0,"L");
$pdf->Cell(200,20,"Max Marks : ".$row1["max_marks"],0,1,"L");
$pdf->Cell(280,20,"  Date : ".$row1["exam_date"],0,1,"L");
$pdf->Cell(540,0.1," ",1,1,"L");
$sql1 = 'SELECT * FROM qp_sections 
WHERE qp_sections .qp_id='.$_GET["qp_id"].' ORDER BY section_title';
$result= mysqli_query($conn, $sql1);
if(mysqli_num_rows($result) > 0) 
{ 
	$pdf->SetFont("Times","B",11);
	$pdf->SetWidths(array(25,300,60,60,120,55));
		$pdf->SetAligns(array("C", "L", "C", "C", "C", "C"));

	$pdf->SetFont("Times","",10);
	$i=1;
	$sec="";
	$l="";
	$pdf->SetWidths(array(25,300,60,60,120,55));
	$pdf->SetAligns(array("C", "C", "C", "C", "C", "C"));
	$pdf->Row2(array("  ",""),10);

	while($row = mysqli_fetch_assoc($result)) 
	{
		$pdf->SetFont("Times","B",10);
		$pdf->SetWidths(array(20,500));
		$pdf->SetAligns(array("C","C"));
		$pdf->Row2(array("","Section - ".$row["section_title"]),20);
		$pdf->SetAligns(array("C","L"));
		$pdf->SetWidths(array(20,500));
		$pdf->SetAligns(array("C","L"));
		$pdf->Row2(array("",$row["section_instruction"]),20);
		if($row["section_type"]=="Optional")
		$pdf->Row2(array($i,"Slove Any ".$row["how_much"]." Out Of ".$row["out_of"]),20);
		else if($row["section_type"]=="OR")
		$pdf->Row2(array($i,"Slove all below questions"),20);
		else
		$pdf->Row2(array($i,"All questions Are Compulsory"),20);
		$pdf->SetWidths(array(25,300,60,60,120,55));
		$pdf->SetAligns(array("C", "C", "C", "C", "C", "C"));
		$pdf->Row2(array("", "", "Max Marks","Co Mapping","Diff Level"),20);
		$sql2 = 'SELECT * FROM questions_details 
		INNER JOIN mst_question ON questions_details.m_ques_id=mst_question.m_ques_id
		WHERE questions_details .qp_sec_id='.$row["qp_sec_id"].' ORDER BY qs_number ';
		$res= mysqli_query($conn, $sql2);
		while($row2 = mysqli_fetch_assoc($res)) 
		{
			$pdf->SetFont("Times","",10);
			$pdf->SetAligns(array("L", "L", "C", "C", "C", "C"));
			$pdf->Row2(array("", " ".$row2["qs_number"]."  ".strip_tags($row2["question"]), " ".$row2["max_marks"], " ".$row2["co_mapping"]," ". $row2["diff_level"]),15);
			if($row2['is_or'] == 1) {
				$pdf->Cell(540,12,"OR",0,1,"C");
			}
		}

		//$l=$row["q_label"];
		$i++;
	}
}
else
	$pdf->Cell(540,30,"No Records available",0,1,"C");
$pdf->Output();
?>