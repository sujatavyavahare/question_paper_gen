function upload_file(id)
{ 
    if ($("#" + id).val() != "")
    {
        var name = document.getElementById(id).files[0].name;
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();
        if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1)
        {
            alert("Invalid Image File... Try to upload valid file...");
            return;
        }
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(id).files[0]);
        var f = document.getElementById(id).files[0];
        var fsize = f.size || f.fileSize;
        if (fsize > 2000000)
        {
            alert("Size Exceeded... Try to upload smaller file...");
            return;
        } else
        {
            form_data.append('file', document.getElementById(id).files[0]);
            $.ajax({
                url: 'ajax.php?call=upload_file&ext='+ext,
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    $("#" + id + "_id").val(data);
                    alert("File Uploaded Successfully");
                }
            });
        }
    }
}