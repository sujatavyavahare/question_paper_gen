<?php 
include "conn.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Dynamic Question Paper Generator - Step 3</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Dynamic Question Paper Generator - Step 3</h2>
                </div>
                <div class="card-body1">
                    <form method="POST" action="process.php?call=step4">
                    <?php 
                    include "conn.php";
                    $sql = 'SELECT q_sec.*,qs.q_no FROM  qp_sections AS q_sec 
                    INNER JOIN (SELECT COUNT(qs_id) AS q_no,qp_sec_id FROM  questions_details
                     GROUP BY qp_sec_id) AS qs ON q_sec.qp_sec_id=qs.qp_sec_id
                    WHERE q_sec.section_type = "Optional" AND q_sec.qp_id = '.$_GET['id'].'
                    ORDER BY q_sec.qp_sec_id';
                    $result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                        echo '<div class="form-row"><div class="name3"></div><div class="name3"></div><div class="name3">Solve Any</div><div class="name3">Out Of</div></div> ';
                        $i=0;
                        $qp_sec_id = "";
                        while($row = mysqli_fetch_assoc($result)) {
                        if($qp_sec_id != $row['qp_sec_id']) {
                            $qp_sec_id = $row['qp_sec_id'];
                            ?>
                            <div class="form-row">
                                <div class="name">Section</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-8" type="text" name="section_title" value="<?php echo $row['section_title']; ?>" readonly>
                                        <input class="input--style-8" type="number" name="how_much<?php echo $i;?>" min="1" max="<?php echo $row['q_no']-1; ?>" >
                                        <input class="input--style-8" type="text" name="out_of<?php echo $i;?>" value="<?php echo $row['q_no']; ?>" readonly >
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                            
                            ?>
                        <input class="name" type="hidden"  name="qp_sec_id<?php echo $i;?>" value="<?php echo $row["qp_sec_id"];?>" >                              
                        <input class="name" type="hidden"  name="qp_id" value="<?php echo $_GET["id"];?>" >                              

                       <?php  $i++;
                        }
                       echo ' <input type="hidden" name="count" id="count" value='.$i.'>';

                    } else {
                        echo "0 results";
                    }
                    
                    ?>
                     <div>
                            </br></br>
                            <button class="btn btn--radius-2 btn--green" type="submit" name="action" value="next" style="margin-left:160px"> Add Next Section </button>
                            <button class="btn btn--radius-2 btn--red" type="submit" name="action" value="save" style="margin-left:40px"> Finish & Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
<!-- end document-->