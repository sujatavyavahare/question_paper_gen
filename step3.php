<?php 
include "conn.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Dynamic Question Paper Generator - Step 3</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Dynamic Question Paper Generator - Step 3</h2>
                </div>
                <div class="card-body1">
                    <form method="POST" action="process.php?call=step3">
                    <?php 
                    include "conn.php";
                    $sql = 'SELECT qs.*, qd.qs_id, q_sec.qp_sec_id, q_sec.section_title FROM mst_question AS qs
                    INNER JOIN questions_details AS qd ON qs.m_ques_id = qd.m_ques_id
                    INNER JOIN qp_sections AS q_sec ON qd.qp_sec_id = q_sec.qp_sec_id
                    WHERE q_sec.section_type = "OR" AND q_sec.qp_id = '.$_GET['id'].'
                    ORDER BY q_sec.qp_sec_id, qd.qs_id';
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        echo '<div class="form-row"><div class="name">Questions </div><div class="name">Ques No</div><div class="name">Mark</div><div class="name">Co- mapp</div><div class="name">Diff Lev</div></div> ';
                        $i=0;
                        $qp_sec_id = "";
                        while($row = mysqli_fetch_assoc($result)) {
                        if($qp_sec_id != $row['qp_sec_id']) {
                            $qp_sec_id = $row['qp_sec_id'];
                            ?>
                            <div class="form-row">
                                <div class="name">Section</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="section_title" value="<?php echo $row['section_title']; ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                            
                            ?>
                        <input class="name" type="hidden"  name="qs_id<?php echo $i;?>" value="<?php echo $row["qs_id"];?>" >
                        <input class="name" type="hidden"  name="qp_id" value="<?php echo $_GET["id"];?>" >                              
                        <div class="form-row">
                        <div class="name"><?php echo $row["question"];?></div>
                        <div class="name"><input class="input--style-6" type="text" readonly ></div>
                        <div class="name"><input class="input--style-6" type="text" readonly ></div>
                        <div class="name"><input class="input--style-6" type="text" readonly ></div>
                        <div class="name"><input class="input--style-6" type="text" readonly ></div></div>
                        <div class="form-row"><div style="margin-left:300px"> <strong>OR</strong> <input class="name" type="checkbox"  name="check<?php echo $i;?>" ></div>
                        </div>
                        
                       <?php  $i++;
                        }
                       echo ' <input type="hidden" name="count" id="count" value='.$i.'>';

                    } else {
                        echo "0 results";
                    }
                    
                    ?>
                    <div>
                        </br></br>
                        <button class="btn btn--radius-2 btn--red" type="submit" name="action" value="save" style="margin-left:330px" > Finish & Save </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
<!-- end document-->