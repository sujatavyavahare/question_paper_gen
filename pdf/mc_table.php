<?php
require('fpdf.php');

class PDF_MC_Table extends FPDF
{
	var $widths;
	var $aligns;
	
	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}
	
	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}
	
	function Rowmenu($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
		{
			 $nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		}	
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
		   $aa=$this->NbLines($this->widths[$i], $data[$i]);
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$new_ht=round ($h/$aa);
				$this->MultiCell($w, $new_ht, $data[$i], 0, 'C');
				
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function Rowmenu1($data,$ht='')
	{
		$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
		{
			 $nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		}	
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
		    $aa=$this->NbLines($this->widths[$i], $data[$i]);
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$new_ht=round ($h/$aa);
			$timedata=explode(",",$data[$i]);
			if($timedata[0]=="Time")
			{
				$this->MultiCell($w, $ht,$timedata[0] , 0, 'C');
				$wth=$w/(count($timedata)-1);
				$this->SetXY($x,$y+$ht);
				for($k=1;$k<count($timedata);$k++)
				{
					$this->Cell($wth, $ht,$timedata[$k] ,1, 0, 'C');
				}	
			}
			else
			$this->MultiCell($w, $new_ht, $data[$i], 0, $a);
			
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function Rowskill($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
		{
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		}	
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$aa=$this->NbLines($this->widths[$i], $data[$i]);
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$new_ht=round ($h/$aa);
			$timedata=explode(",",$data[$i]);
			// below is special casae never use or edit it 
			if(($timedata[0]=="Experience11"))
			{
				$this->MultiCell($w, $ht,$timedata[0] , 0, 'C');
				$wth=$w/(count($timedata)-1);
				$this->SetXY($x,$y+$ht);
				for($k=1;$k<count($timedata);$k++)
				{
					$this->Cell($wth, $ht,$timedata[$k] ,1, 0, 'C');
				}	
			}
			else
			$this->MultiCell($w, $new_ht, $data[$i], 0, $a);
			
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Rowskill1($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=3;
		for($i=0;$i<count($data);$i++)
		{
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		}	
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
		   $aa=$this->NbLines($this->widths[$i], $data[$i]);
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$new_ht=round ($h/$aa);
			$timedata=explode(",",$data[$i]);
			// below is special casae never use or edit it 
			if(($timedata[0]=="Experience11"))
			{
				$this->MultiCell($w, $ht,$timedata[0] , 0, 'C');
				$wth=$w/(count($timedata)-1);
				$this->SetXY($x,$y+$ht);
				for($k=1;$k<count($timedata);$k++)
				{
					$this->Cell($wth, $ht,$timedata[$k] ,1, 0, 'C');
				}	
			}
			else
			$this->MultiCell($w, $new_ht, $data[$i], 0, $a);
			
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	
	function Rowskill_NL($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
		{
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		}	
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
		   $aa=$this->NbLines($this->widths[$i], $data[$i]);
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$new_ht=round ($h/$aa);
			$timedata=explode(",",$data[$i]);
			// below is special casae never use or edit it 
			if(($timedata[0]=="Experience11"))
			{
				$this->MultiCell($w, $ht,$timedata[0] , 0, 'C');
				$wth=$w/(count($timedata)-1);
				$this->SetXY($x,$y+$ht);
				for($k=1;$k<count($timedata);$k++)
				{
					$this->Cell($wth, $ht,$timedata[$k] ,1, 0, 'C');
				}	
			}
			else
			$this->MultiCell($w, $new_ht, $data[$i], 0, $a);
			
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		//$this->Ln($h);
	}
	
	function Rowmenu11($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
		{
			 $nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		}	
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
		   $aa=$this->NbLines($this->widths[$i], $data[$i]);
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$new_ht=round ($h/$aa);
			$timedata=explode(",",$data[$i]);
			if($timedata[0]=="Observations")
			{
				$this->MultiCell($w, $ht,$timedata[0] , 0, 'L');
				$wth=$w/(count($timedata)-1);
				$this->SetXY($x,$y+$ht);
				for($k=1;$k<count($timedata);$k++)
				{
					$this->Cell($wth, $ht,$timedata[$k] ,1, 0, 'L');
				}	
			}
			else
			$new_ht1=($new_ht-$new_ht)+10;
			$this->MultiCell($w, $new_ht1, $data[$i], 0, $a);
			
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function Row($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function Row2($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	function Row7($data,$ht='')
	{
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','11');
		
		//$this->Ln($h);
	}
	function Row1($data,$ht='')
	{
		$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			$timedata=explode(",",$data[$i]);
			if($timedata[0]=="Time")
			{
				$this->MultiCell($w, $ht,$timedata[0] , 0, 'C');
				$wth=$w/(count($timedata)-1);
				$this->SetXY($x,$y+$ht);
				for($k=1;$k<count($timedata);$k++)
				{
					$this->Cell($wth, $ht,$timedata[$k] ,1, 0, 'C');
				}	
			}
			else
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		$this->Ln($h);
	}
	
	function Row_NB_BN_Font($data,$ht='')
	{
		$this->SetFont('Arial','','11');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','11');
		}
		//Go to the next line
		$this->Ln($h);
		
	}
	
	function Row_NB_BN_Font1($data,$ht='')
	{
		$this->SetFont('Arial','B','8.5');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','8.5');
		}
		//Go to the next line
		$this->Ln($h);
		
	}
	
	function Row_NB_BN_L($data,$ht='')
	{
		//$this->SetFont('Arial','B','10');
		$this->SetLeftMargin(40);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','10');
		}
		
		//Go to the next line
		//$this->Ln($h);
	}
	
	function Row_NB_BN_L1($data,$ht='')
	{
		//$this->SetFont('Arial','B','10');
		$this->SetLeftMargin(40);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','10');
		}
		
		//Go to the next line
		//$this->Ln($h);
	}
	
	function Row_NB_BN_C($data,$ht='')
	{
		//$this->SetFont('Arial','B','10');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','10');
		}
		//Go to the next line
		//$this->Ln($h);
		
	}
		
	function Row_Normal_L($data,$ht='')
	{
		$this->SetFont('Arial','','10');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','10');
		}
		//Go to the next line
		$this->Ln($h);
		
	}
	
	function Row_NoBorder_Bold($data,$ht='')
	{
		$this->SetFont('Arial','B','11');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','11');
		}
		//Go to the next line
		//$this->Ln($h);
	}
	
	function Row_NoBorder_Normal($data,$ht='')
	{
		$this->SetFont('Arial','','11');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			$timedata=explode(",",$data[$i]);
			if($timedata[0]=="Dt. & Time Rem.")
			{
				$this->SetFont('Arial','','9');			
			}
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','11');
		}
		//Go to the next line
		//$this->Ln($h);
	}
	
	function footer_revision_P($data,$ht='')
	{
		$this->SetFont('Arial','','10');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'R';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
			$this->SetFont('Arial','','11');
		}
		//Go to the next line
		//$this->Ln($h);
	}
	
	function footer_revision_L($data,$ht='')
	{
		$this->Cell(180,10,'',0,1,'');
		$this->SetFont('Arial','','10');
		//$this->SetLeftMargin(20);
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
		$h=$ht*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'R';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x, $y, $w, $h);
			//Print the text
			
			$this->MultiCell($w+130, $ht, $data[$i], 0, $a);	
			//Put the position to the right of the cell
			$this->SetXY($x+$w, $y);
		}
		//Go to the next line
		//$this->Ln($h);
	}
	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h+40>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}
	
	function NbLines($w, $txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r", '', $txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
	function Circle($x, $y, $r, $style='')
	{
		$this->Ellipse($x, $y, $r, $r, $style);
	}
	
	function Ellipse($x, $y, $rx, $ry, $style='D')
	{
		if($style=='F')
			$op='f';
		elseif($style=='FD' or $style=='DF')
			$op='B';
		else
			$op='S';
		$lx=4/3*(M_SQRT2-1)*$rx;
		$ly=4/3*(M_SQRT2-1)*$ry;
		$k=$this->k;
		$h=$this->h;
		$this->_out(sprintf('%.2f %.2f m %.2f %.2f %.2f %.2f %.2f %.2f c',
			($x+$rx)*$k, ($h-$y)*$k,
			($x+$rx)*$k, ($h-($y-$ly))*$k,
			($x+$lx)*$k, ($h-($y-$ry))*$k,
			$x*$k, ($h-($y-$ry))*$k));
		$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c',
			($x-$lx)*$k, ($h-($y-$ry))*$k,
			($x-$rx)*$k, ($h-($y-$ly))*$k,
			($x-$rx)*$k, ($h-$y)*$k));
		$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c',
			($x-$rx)*$k, ($h-($y+$ly))*$k,
			($x-$lx)*$k, ($h-($y+$ry))*$k,
			$x*$k, ($h-($y+$ry))*$k));
		$this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c %s',
			($x+$lx)*$k, ($h-($y+$ry))*$k,
			($x+$rx)*$k, ($h-($y+$ly))*$k,
			($x+$rx)*$k, ($h-$y)*$k,
			$op));
	}
}
?>