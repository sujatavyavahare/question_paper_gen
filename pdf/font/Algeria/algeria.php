<?php
$type='TrueType';
$name='Algerian';
$desc=array('Ascent'=>888,'Descent'=>-224,'CapHeight'=>667,'Flags'=>32,'FontBBox'=>'[-188 -259 1017 888]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>250);
$up=-118;
$ut=57;
$cw=array(
	chr(0)=>250,chr(1)=>250,chr(2)=>250,chr(3)=>250,chr(4)=>250,chr(5)=>250,chr(6)=>250,chr(7)=>250,chr(8)=>250,chr(9)=>250,chr(10)=>250,chr(11)=>250,chr(12)=>250,chr(13)=>250,chr(14)=>250,chr(15)=>250,chr(16)=>250,chr(17)=>250,chr(18)=>250,chr(19)=>250,chr(20)=>250,chr(21)=>250,
	chr(22)=>250,chr(23)=>250,chr(24)=>250,chr(25)=>250,chr(26)=>250,chr(27)=>250,chr(28)=>250,chr(29)=>250,chr(30)=>250,chr(31)=>250,' '=>250,'!'=>323,'"'=>338,'#'=>586,'$'=>586,'%'=>626,'&'=>802,'\''=>192,'('=>444,')'=>444,'*'=>350,'+'=>587,
	','=>262,'-'=>314,'.'=>267,'/'=>480,'0'=>587,'1'=>600,'2'=>600,'3'=>600,'4'=>600,'5'=>600,'6'=>600,'7'=>600,'8'=>600,'9'=>600,':'=>267,';'=>262,'<'=>500,'='=>587,'>'=>500,'?'=>434,'@'=>720,'A'=>757,
	'B'=>646,'C'=>582,'D'=>604,'E'=>622,'F'=>551,'G'=>682,'H'=>628,'I'=>306,'J'=>513,'K'=>677,'L'=>547,'M'=>731,'N'=>601,'O'=>624,'P'=>591,'Q'=>641,'R'=>635,'S'=>555,'T'=>579,'U'=>607,'V'=>620,'W'=>790,
	'X'=>698,'Y'=>642,'Z'=>641,'['=>438,'\\'=>517,']'=>438,'^'=>500,'_'=>500,'`'=>500,'a'=>250,'b'=>250,'c'=>250,'d'=>250,'e'=>250,'f'=>250,'g'=>250,'h'=>250,'i'=>250,'j'=>250,'k'=>250,'l'=>250,'m'=>250,
	'n'=>250,'o'=>250,'p'=>250,'q'=>250,'r'=>250,'s'=>250,'t'=>250,'u'=>250,'v'=>250,'w'=>250,'x'=>250,'y'=>250,'z'=>250,'{'=>440,'|'=>500,'}'=>440,'~'=>667,chr(127)=>250,chr(128)=>587,chr(129)=>250,chr(130)=>262,chr(131)=>600,
	chr(132)=>411,chr(133)=>800,chr(134)=>588,chr(135)=>588,chr(136)=>500,chr(137)=>939,chr(138)=>555,chr(139)=>201,chr(140)=>953,chr(141)=>250,chr(142)=>250,chr(143)=>250,chr(144)=>250,chr(145)=>220,chr(146)=>220,chr(147)=>414,chr(148)=>411,chr(149)=>667,chr(150)=>514,chr(151)=>714,chr(152)=>500,chr(153)=>875,
	chr(154)=>250,chr(155)=>207,chr(156)=>250,chr(157)=>250,chr(158)=>250,chr(159)=>642,chr(160)=>250,chr(161)=>310,chr(162)=>586,chr(163)=>600,chr(164)=>587,chr(165)=>600,chr(166)=>500,chr(167)=>546,chr(168)=>500,chr(169)=>784,chr(170)=>391,chr(171)=>302,chr(172)=>500,chr(173)=>314,chr(174)=>784,chr(175)=>500,
	chr(176)=>303,chr(177)=>587,chr(178)=>367,chr(179)=>367,chr(180)=>500,chr(181)=>461,chr(182)=>635,chr(183)=>179,chr(184)=>500,chr(185)=>367,chr(186)=>405,chr(187)=>306,chr(188)=>857,chr(189)=>857,chr(190)=>857,chr(191)=>434,chr(192)=>757,chr(193)=>757,chr(194)=>757,chr(195)=>757,chr(196)=>757,chr(197)=>757,
	chr(198)=>1007,chr(199)=>592,chr(200)=>622,chr(201)=>622,chr(202)=>622,chr(203)=>622,chr(204)=>306,chr(205)=>306,chr(206)=>306,chr(207)=>306,chr(208)=>604,chr(209)=>601,chr(210)=>624,chr(211)=>624,chr(212)=>624,chr(213)=>624,chr(214)=>624,chr(215)=>587,chr(216)=>625,chr(217)=>607,chr(218)=>607,chr(219)=>607,
	chr(220)=>607,chr(221)=>642,chr(222)=>578,chr(223)=>250,chr(224)=>250,chr(225)=>250,chr(226)=>250,chr(227)=>250,chr(228)=>250,chr(229)=>250,chr(230)=>250,chr(231)=>250,chr(232)=>250,chr(233)=>250,chr(234)=>250,chr(235)=>250,chr(236)=>250,chr(237)=>250,chr(238)=>250,chr(239)=>250,chr(240)=>250,chr(241)=>250,
	chr(242)=>250,chr(243)=>250,chr(244)=>250,chr(245)=>250,chr(246)=>250,chr(247)=>587,chr(248)=>250,chr(249)=>250,chr(250)=>250,chr(251)=>250,chr(252)=>250,chr(253)=>250,chr(254)=>250,chr(255)=>250);
$enc='cp1252';
$diff='';
$file='Algeria/d75bca4013887af52c9b6b56d3702b53_alger.z';
$originalsize=75272;
?>