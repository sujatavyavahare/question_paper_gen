<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Add Question </title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Question List </h2>
                </div>
                <div class="card-body1">
                <?php 
                $conn = mysqli_connect("localhost", "root", "", "question_paper") or die ("Unable to connect to the database check again..!!");
                    $sql = 'SELECT * FROM mst_question ';
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        echo '<div class="form-row"><div class="name1">No.</div><div class="name1">Questions Type</div><div class="name1">Subject</div><div class="name2">Question</div></div> ';
                        $i=0;
                        while($row = mysqli_fetch_assoc($result)) {?>
                        <div class="form-row1"><div class="input--style-6"><?php echo $i+1;?></div>
                        <div class="input--style-6"><?php echo $row["ques_type"];?></div>
                        <div class="input--style-6"><?php echo $row["subject_ques"];?></div>
                        <div class="input--style-7"><?php echo $row["question"];?></div></div><br>
                        
                       <?php  $i++;
                        }
                       echo ' <input type="hidden" name="count" id="count" value='.$i.'>';

                    }  else {
                        echo "0 results";
                    }
                ?>                               
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    <script src="js/custom.js"></script>
    <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
.
.
.


</body>

</html>
<!-- end document-->