<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Dynamic Question Paper Generator - Step 2</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                <h2 class="title">Dynamic Question Paper Generator - Step 2</h2>
                </div>
                <div class="card-body1">
                    <form method="POST" action="process.php?call=step2">
                    <?php if(isset($_GET['id'])) { ?>
                    <input type="hidden" name="qp_id" id="qp_id" value="<?php echo $_GET['id']; ?>">
                    <?php } else { ?>
                    <div class="form-row">
                        <div class="name">Select Exam</div>
                        <div class="value">
                            <div class="input-group">
                                <select class="input--style-5" name="qp_id" id="qp_id" style="height:35px" required >
                                <?php 
                                $conn = mysqli_connect("localhost", "root", "", "question_paper") or die ("Unable to connect to the database check again..!!");
                                $sql = 'SELECT * FROM qp_details WHERE qp_id NOT IN (SELECT qp_id FROM questions_details)';
                                $res = mysqli_query($conn, $sql);
                                echo "<option>SELECT</option>";
                                while($row1 = mysqli_fetch_assoc($res)) {
                                    echo '<option value='.$row1["qp_id"].'>'.$row1["exam_name"].'-'.$row1["course_name"],'</option>';   
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div> 
                    <?php } ?>
                    <div class="form-row">
                        <div class="name">Section</div>
                        <div class="value">
                            <div class="input-group">
                                <input class="input--style-5" type="text" name="section_title" placeholder="e.g. A or B" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="name">Special Instruction</div>
                        <div class="value">
                            <div class="input-group">
                                <textarea class="input--style-5" name="section_instruction" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="name">Section Type</div>
                        <div class="value">
                            <div class="input-group">
                                <select class="input--style-5" name="section_type" style="height:35px" >
                                <option></option>                                
                                <option value="Mandatory">Mandatory</option>
                                <option value="OR">OR</option>
                                <option value="Optional">Optional</option>
                                </select>
                            </div>
                        </div>
                    </div>    
                    <div style="width: 850px; height: 280px; overflow-y: scroll;">
                    <?php 
                    include "conn.php";
                    $sql = 'SELECT * FROM mst_question ';
                    $result = mysqli_query($conn, $sql);

                    if (mysqli_num_rows($result) > 0) {
                        echo '<div class="form-row"><div class="name"> </div><div class="name">Questions </div><div class="name">Ques No</div><div class="name">Mark</div><div class="name">Co- mapp</div><div class="name">Diff Lev</div></div> ';
                        $i=0;
                        while($row = mysqli_fetch_assoc($result)) {?>
                        <input class="name" type="hidden"  name="m_ques_id<?php echo $i;?>" value="<?php echo $row["m_ques_id"];?>" >
                        <div class="form-row"><div ><input class="name" type="checkbox"  name="check<?php echo $i;?>" ></div>
                        <div class="name"><?php echo $row["question"];?></div>
                        <div class="name"><input class="input--style-6" type="text" name="qs_number<?php echo $i;?>"  ></div>
                        <div class="name"><input class="input--style-6" type="text" name="max_marks<?php echo $i;?>"  ></div>
                        <div class="name"><input class="input--style-6" type="text" name="co_mapping<?php echo $i;?>"  ></div>
                        <div class="name"><input class="input--style-6" type="text" name="diff_level<?php echo $i;?>"  ></div></div><br>
                        
                       <?php  $i++;
                        }
                       echo ' <input type="hidden" name="count" id="count" value='.$i.'>';

                    } else {
                        echo "0 results";
                    }
                    
                    ?>
                    </div>  
                        <div>
                            </br></br>
                            <button class="btn btn--radius-2 btn--green" type="submit" name="action" value="next" style="margin-left:160px"> Add Next Section </button>
                            <button class="btn btn--radius-2 btn--red" type="submit" name="action" value="save" style="margin-left:40px"> Finish & Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
<!-- end document-->