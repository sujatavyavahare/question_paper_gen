<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Home</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Home</h2>
                </div>
                <div class="card-body">
                    <div>
                        <button class="btn btn--radius-2 btn--red" ><a href="ques_add.php">Add Question</a></button>
                    </div>
                    </br>
                    <div>
                        <button class="btn btn--radius-2 btn--red" type="submit"><a href="ques_list.php">View Questions</a></button>
                    </div>
                    </br>
                    <div>
                        <button class="btn btn--radius-2 btn--red" type="submit"><a href="step1.php">Add Paper Master Details</a></button>
                    </div>
                    </br>
                    <div>
                        <button class="btn btn--radius-2 btn--red" type="submit"><a href="step2.php">Add Question Paper</a></button>
                    </div>
                    </br>
                    <div>
                        <button class="btn btn--radius-2 btn--red" type="submit"><a href="qp_list.php">View Question Papers</a></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>
<!-- end document-->